package com.wqj.ride.happyriding.utils;

import android.content.Context;

import com.amap.api.maps.CoordinateConverter;
import com.amap.api.maps.model.LatLng;

/**
 * Created by Administrator on 2017/12/23.
 */

public final class UnitConverter {

    public static String TAG = "UnitConverter";

    public static LatLng GPS2AMap(Context context, LatLng gps){

        LatLng amap;
        CoordinateConverter converter = new CoordinateConverter(context);
        converter.from(CoordinateConverter.CoordType.GPS);
        converter.coord(gps);

        amap = converter.convert();

        try {

        } catch ( Exception e) {
            android.util.Log.e(TAG, "GPS to AMap failed.");
        }

        return amap;
    }
}
