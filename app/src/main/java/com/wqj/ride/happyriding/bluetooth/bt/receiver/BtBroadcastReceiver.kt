package com.wqj.ride.happyriding.bluetooth.bt.receiver

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.wqj.ride.happyriding.bluetooth.bt.BtDevicesActivity
import com.wqj.ride.happyriding.bluetooth.bt.bean.BluetoothDeviceDean
import com.wqj.ride.happyriding.bluetooth.bt.manager.BtManager

/**
 * Created by kyee on 17-12-8.
 */
class BtBroadcastReceiver(activity: BtDevicesActivity): BroadcastReceiver() {
    internal var pin = "1234"
    var activity: BtDevicesActivity
    init {
        this.activity = activity
    }
    override fun onReceive(context: Context, intent: Intent) {

        val action = intent.action
        Log.e("TAG", "action: " + action)
        var btDevice: BluetoothDevice? = null

        if (BluetoothDevice.ACTION_FOUND == action) {
            btDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
            if (btDevice == null || btDevice!!.name == null) {
                return
            }
            Log.d(TAG, "found [" + btDevice!!.name + "]" + ":" + btDevice.address)

            if (btDevice.name.contains(BtDevicesActivity.BT_DEVICE_NAME)) {
                var bluetoothDeviceDean: BluetoothDeviceDean
                var it: Iterator<BluetoothDeviceDean> = activity.mScanedBtDevices.iterator()

                // 拒绝重复数据加入列表
                while (it.hasNext()) {
                    bluetoothDeviceDean = it.next()
                    if (bluetoothDeviceDean.bluetoothDevice.address.equals(btDevice.address)) {
                        return
                    }
                }

                bluetoothDeviceDean = BluetoothDeviceDean(btDevice)
                activity.mScanedBtDevices.add(bluetoothDeviceDean)

                activity.mBtDevicesAdapter.notifyDataSetChanged()
                Log.d(TAG, "found2 [" + btDevice!!.name + "]" + ":" + btDevice.address)

                if (btDevice.bondState == BluetoothDevice.BOND_NONE) {

                    Log.d(TAG, "attemp to bond:" + "[" + btDevice.name + "]")
                    //BTDevicesActivity.bluetoothAdapter.cancelDiscovery()
                    try {
                        //ClsUtil.createBond(btDevice.javaClass, btDevice)
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                }
                if (!BtManager.getInstance().isConnected) {
                    // 建立RFCOMM连接
                    // btManager.connect(bluetoothDeviceDean)
                }

            } else {
                Log.d("wqj", "该蓝牙设备不是Happy Ridding设备")
            }
        } else if (action == "android.bluetooth.device.action.PAIRING_REQUEST") {
            /*
            btDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
            if (btDevice == null) {
                return
            }
            Log.d(TAG, "action: " + action)
            if (btDevice!!.name.contains(BtDevicesActivity.BT_DEVICE_NAME)) {
                try {
                    ClsUtil.setPairingConfirmation(btDevice.javaClass, btDevice, true)
                    abortBroadcast()
                    val ret = ClsUtil.setPin(btDevice.javaClass, btDevice, pin)

                } catch (e: Exception) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

            } else
                Log.e("wqj", "paring failed")
            */
        } else if (action == BluetoothDevice.ACTION_BOND_STATE_CHANGED) {

        } else if (action == BluetoothDevice.ACTION_ACL_CONNECTED) {
            var text = "已连接"
            //Toast.makeText(c, text, Toast.LENGTH_SHORT).show()
        } else if (action == BluetoothDevice.ACTION_ACL_DISCONNECTED) {
            var text = "连接已断开"
            //Toast.makeText(c, text, Toast.LENGTH_SHORT).show()
        } else if (action == BluetoothAdapter.ACTION_DISCOVERY_FINISHED) {
            // MainActivity.startActivity<BTDevicesActivity>()
        }else if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            /*
            var bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if(bluetoothAdapter.isEnabled){
                activity.bluetoothAdapter.startDiscovery()
            }else{

            }
            */
        }
    }
    companion object {
        private val TAG: String = "BtBroadcastReceiver"
    }
}