package com.wqj.ride.happyriding.bean

/**
 * Created by kyee on 18-1-10.
 */
class DataBean {
    data class Route(var title: String, var image: String)

    companion object {
        var maxSpeed: Float = 0f                        // 骑行时的最大速度
        var speed: ArrayList<Float> = arrayListOf()     // 记录实时速度
        var distance: Float = 0f                        // 本次骑行距离
        var rideTime: Long = 0                          // 本次骑行时长
        var startTime: Long = 0                         // 开始骑行时间点
        var endTime: Long = 0                           // 完成骑行时间点
    }
}