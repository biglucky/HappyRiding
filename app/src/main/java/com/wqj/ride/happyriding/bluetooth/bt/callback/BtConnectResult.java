package com.wqj.ride.happyriding.bluetooth.bt.callback;

public interface BtConnectResult {
    void onSuccess();
    void onFailure();
}
