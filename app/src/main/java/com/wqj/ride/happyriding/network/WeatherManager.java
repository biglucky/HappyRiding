package com.wqj.ride.happyriding.network;

import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.wqj.ride.happyriding.network.listener.WeatherResponseListener;
import com.wqj.ride.happyriding.network.request.WeatherRequest;
import com.wqj.ride.happyriding.network.response.WeatherResponse;
import com.wqj.ride.happyriding.network.schedulers.SchedulerProvider;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherManager {
    private static final String TAG = "WeatherManager";

    private static volatile WeatherManager mInstance;
    private static Retrofit mRetrofit;

    private AMapLocation mLocation; // 当前位置

    public WeatherManager() {
        init();
    }

    public void init() {
        // add okhttp log
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        // init okhttp
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();


        // init retrofit
        mRetrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(WeatherRequest.HOST)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static WeatherManager getInstance() {
        if (null == mInstance) {
            synchronized (WeatherManager.class) {
                if (null == mInstance) {
                    mInstance = new WeatherManager();
                }
            }
        }
        return mInstance;
    }

    public void getWeather(String cityName, WeatherResponseListener listener) {
        WeatherRequest request = mRetrofit.create(WeatherRequest.class);
        Observable<WeatherResponse> observable = request.getWeather("101021200");

        observable
                .compose(SchedulerProvider.applySchedulers())
                .subscribe(new Observer<WeatherResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(WeatherResponse w) {
                        listener.onResponse(w.getWeatherinfo());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public String getCityId(String cityName) {

        return "101110101";
    }

    public AMapLocation getLocation() {
        return mLocation;
    }

    public void setLocation(AMapLocation mLocation) {
        this.mLocation = mLocation;
    }


}

