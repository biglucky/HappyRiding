package com.wqj.ride.happyriding.nav.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.wqj.ride.happyriding.R;

import java.util.ArrayList;

/**
 * Created by wqj on 2018/1/1.
 */

public class ItemSimpleAdapter extends BaseAdapter {

    private ArrayList<PoiItem> mList;
    private Context mContext;

    public ItemSimpleAdapter(ArrayList<PoiItem> list, Context context) {
        mList = list;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ((long) i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHodler holder;
        PoiItem info = mList.get(position);

        View v;
        if (convertView == null) {
            holder = new MyHodler();
            v = LayoutInflater.from(mContext).inflate(R.layout.search_result_item, parent
                    , false);
            holder.info1 = (TextView) v.findViewById(R.id.name);
            holder.info2 = (TextView) v.findViewById(R.id.adress);
            v.setTag(holder);
        } else {
            v = convertView;
            holder = (MyHodler) v.getTag();
        }

        holder.info1.setText(info.getTitle());
        holder.info2.setText(info.getSnippet());

        return v;
    }

    class MyHodler {
        public TextView info1;
        public TextView info2;
    }
}
