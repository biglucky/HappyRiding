package com.wqj.ride.happyriding.service.listener;

public interface TimerListener {
    void onTimerUpdate(Long value);
}
