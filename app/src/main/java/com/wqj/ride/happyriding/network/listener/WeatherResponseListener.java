package com.wqj.ride.happyriding.network.listener;

import com.wqj.ride.happyriding.network.bean.WeatherInfo;

public abstract class WeatherResponseListener {
    public abstract void onResponse(WeatherInfo t);
}
