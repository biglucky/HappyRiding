package com.wqj.ride.happyriding.network.listener;

public abstract class HttpResponseListener<T> {

    public abstract void onResponse(T t);
}
