package com.wqj.ride.happyriding.network.response;

import com.wqj.ride.happyriding.network.bean.WeatherInfo;

public class WeatherResponse {
    WeatherInfo weatherinfo;

    public WeatherInfo getWeatherinfo() {
        return weatherinfo;
    }

    public void setWeatherinfo(WeatherInfo weatherinfo) {
        this.weatherinfo = weatherinfo;
    }
}
