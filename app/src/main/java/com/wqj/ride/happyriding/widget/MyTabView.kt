package com.wqj.ride.happyriding.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import com.wqj.ride.happyriding.R
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.dip
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor

/**
 * Created by kyee on 17-12-4.
 */
class MyTabView(context: Context) : TextView(context) {
    var normalDrawable: Drawable? = null
    var selectedDrawable: Drawable? = null

    init {
        var layoutParams = LinearLayout.LayoutParams(dip(50),
                LinearLayout.LayoutParams.MATCH_PARENT, 1f)
        layoutParams.weight = 1f
        this.layoutParams = layoutParams
        padding = dip(5)
        compoundDrawablePadding = dip(1)
        textSize = 12f
        gravity = Gravity.CENTER
    }

    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
        Log.d("wqj", "selected: " + selected)
        if (selected) {
            this.backgroundColor = ContextCompat.getColor(context, R.color.colorTabSelected)
            this.textColor = ContextCompat.getColor(context, R.color.colorTabTextSelected)

            if (selectedDrawable != null) {
                this.setCompoundDrawablesWithIntrinsicBounds(null, selectedDrawable, null, null)
            }
            Log.d("wqj", "draw1: " + selectedDrawable)
        } else {
            this.backgroundColor = ContextCompat.getColor(context, android.R.color.transparent)
            this.textColor = ContextCompat.getColor(context, R.color.colorTabTextNormal)
            if (normalDrawable != null) {
                this.setCompoundDrawablesWithIntrinsicBounds(null, normalDrawable, null, null)
            }
            Log.d("wqj", "draw2: " + normalDrawable)
        }
    }
}