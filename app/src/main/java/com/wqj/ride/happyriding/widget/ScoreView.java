package com.wqj.ride.happyriding.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.wqj.ride.happyriding.R;

/**
 * TODO: document your custom view class.
 */
public class ScoreView extends ConstraintLayout {
    private String mValue;
    private String mKey;


    public ScoreView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public ScoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public ScoreView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        View v = LayoutInflater.from(context).inflate(R.layout.sample_score_view, this);

        TextView keyView = v.findViewById(R.id.keyView);
        TextView valueView = v.findViewById(R.id.valueView);

        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.ScoreView, defStyle, 0);

        if (a.hasValue(R.styleable.ScoreView_key)) {
            mKey = a.getString(R.styleable.ScoreView_key);
            keyView.setText(mKey);
        }
        if (a.hasValue(R.styleable.ScoreView_value)) {
            mValue = a.getString(R.styleable.ScoreView_value);
            valueView.setText(mValue);
        }
        Log.d("hello", "mkey: " + mKey + ", view: " + keyView);
        a.recycle();


    }



}
