package com.wqj.ride.happyriding.bluetooth.bt.bean;

import android.bluetooth.BluetoothDevice;

/**
 * Created by rabby on 2017/12/17.
 */

public class BluetoothDeviceDean{

    private BluetoothDevice bluetoothDevice;

    private  boolean isContected;

    public BluetoothDeviceDean(BluetoothDevice bluetoothDevice){
        this.bluetoothDevice = bluetoothDevice;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public boolean isContected() {
        return isContected;
    }

    public void setContected(boolean contected) {
        isContected = contected;
    }
}
