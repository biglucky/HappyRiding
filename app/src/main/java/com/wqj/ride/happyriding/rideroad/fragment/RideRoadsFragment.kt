package com.wqj.ride.happyriding.rideroad.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.wqj.ride.happyriding.R
import com.wqj.ride.happyriding.utils.Tool
import com.wqj.ride.happyriding.bean.DataBean
import com.wqj.ride.happyriding.rideroad.adapter.RideRoadsListViewAdapter
import kotlinx.android.synthetic.main.fragment_ride_roads.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RideRoadsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RideRoadsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RideRoadsFragment : Fragment() {

    // TODO: Rename and change types of parameters
    var routeList: ArrayList<DataBean.Route> = arrayListOf()
    private var mParam1: String? = null
    private var mParam2: String? = null
    lateinit var listView: ListView
    lateinit var mAdapter: RideRoadsListViewAdapter

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d("wqj", "createView")
        var view = LayoutInflater.from(context).inflate(R.layout.fragment_ride_roads, null, false)
        listView = view.listView
        mAdapter = RideRoadsListViewAdapter(routeList, this.context)
        listView.adapter = mAdapter
        return view
        /*
        return UI {
            verticalLayout {
                padding = dip(0)
                backgroundColor = ContextCompat.getColor(ctx, android.R.color.white)
                listView = listView() {
                    adapter = RideRoadsListViewAdapter(routeList, ctx)
                    padding = dip(0)
                    onItemClickListener = object : AdapterView.OnItemClickListener {
                        override fun onItemClick(parent: AdapterView<*>?, v: View?, position: Int, id: Long) {
                            when (position) {
                                0 -> {
                                    // startActivity<NfcActivity>()
                                }
                            }
                        }
                    }
                }.lparams(width = matchParent) {
                    height = matchParent
                    margin = dip(0)
                }
            }
        }.view
        */
    }

    override fun onStart() {
        super.onStart()
        Log.d("wqj", "start")

        getRoutes(0, 10)
    }

    fun getRoutes(start: Int, count: Int) {

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RideRoadsFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): RideRoadsFragment {
            val fragment = RideRoadsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
