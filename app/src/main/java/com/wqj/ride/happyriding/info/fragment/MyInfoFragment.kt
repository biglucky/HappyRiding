package com.wqj.ride.happyriding.info.fragment

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amap.api.location.AMapLocationClient
import com.amap.api.location.AMapLocationClientOption
import com.amap.api.location.AMapLocationListener
import com.wqj.ride.happyriding.R
import com.wqj.ride.happyriding.bluetooth.bt.BtDevicesActivity
import com.wqj.ride.happyriding.network.WeatherManager
import com.wqj.ride.happyriding.network.bean.WeatherInfo
import com.wqj.ride.happyriding.network.listener.WeatherResponseListener
import com.wqj.ride.happyriding.service.HappyRiddingService
import kotlinx.android.synthetic.main.fragment_my_info.*
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.startActivity

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MyInfoFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MyInfoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MyInfoFragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    lateinit var mLocationClient: AMapLocationClient
    lateinit var mLocationListener: AMapLocationListener

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }


        mLocationClient = AMapLocationClient(context)
        // 定位回调函数
        mLocationListener = AMapLocationListener { location ->
            // 定位一次得到城市名称
            mLocationClient.stopLocation()
            Log.d(TAG, "location: " + location.toString())
            WeatherManager.getInstance().setLocation(location)
            WeatherManager.getInstance().getWeather(location.city
                    , object : WeatherResponseListener() {
                override fun onResponse(w: WeatherInfo) {
                    // tempView.text = "当前气温: " + w.temp + "度"
                    // windView.text = "当前风向: " + w.wd + "\n当前风力: " + w.ws
                    Log.d(TAG, "weather: " + w.toString())
                }
            })
        }
        mLocationClient.setLocationListener(mLocationListener)
        val option = AMapLocationClientOption()
        option.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy)
        option.setOnceLocationLatest(true)

        mLocationClient.stopLocation()
        mLocationClient.startLocation()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_my_info, null
                , false)
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        btDeviceView.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                startActivity<BtDevicesActivity>()
            }

        })
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val TAG: String = "MyInfo"
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MyInfoFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): MyInfoFragment {
            val fragment = MyInfoFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
