package com.wqj.ride.happyriding.service.listener;

import com.amap.api.navi.model.NaviInfo;

public interface NaviInfoUpdateListener {
    void onNaviInfoUpdate(NaviInfo naviInfo);
}
