package com.wqj.ride.happyriding.bluetooth.bt.ui

import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import android.view.ViewManager
import android.widget.AdapterView
import com.wqj.ride.happyriding.R
import com.wqj.ride.happyriding.bluetooth.bt.BtDevicesActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView

/**
 * Created by kyee on 17-12-8.
 */
class BtDevicesUi : AnkoComponent<BtDevicesActivity> {

    public inline fun ViewManager.floatingActionButton() = floatingActionButton {}
    public inline fun ViewManager.floatingActionButton(init: FloatingActionButton.() -> Unit)
            = ankoView({ FloatingActionButton(it) }, 0, init)

    override fun createView(ui: AnkoContext<BtDevicesActivity>): View {
        return with(ui) {
            frameLayout {

                // backgroundColor = ContextCompat.getColor(ui.ctx, android.R.color.black)
                background = ContextCompat.getDrawable(ctx, R.mipmap.bgmohu)
                relativeLayout {
                    backgroundColor = ContextCompat.getColor(ctx, R.color.colorPrimary)
                    textView {
                        id = ID_TITLE
                        text = "扫描蓝牙"
                        textSize = 16f
                        textColor = ContextCompat.getColor(ctx, R.color.colorRidingInfoText)
                        gravity = Gravity.CENTER
                    }.lparams {
                        width = matchParent
                        height = matchParent
                        gravity = Gravity.CENTER
                    }
                    imageView {
                        id = ID_BACK_BTN
                        background = ContextCompat.getDrawable(ctx, R.drawable.action_bar_back)
                    }.lparams {
                        width = dip(30)
                        height = dip(30)
                        rightMargin = dip(10)
                        leftMargin = dip(10)
                        centerVertically()
                        alignParentStart()
                    }
                }.lparams {
                    width = matchParent
                    height = dip(50)
                }

                listView() {
                    id = ID_BT_DEVICES_LIST
                    onItemClickListener = object : AdapterView.OnItemClickListener {
                        override fun onItemClick(parent: AdapterView<*>?, v: View?
                                                 , position: Int, id: Long) {
                            when (position) {
                                0 -> {
                                    // startActivity<BTDevicesActivity>()
                                }
                            }
                        }
                    }
                }.lparams(width = matchParent) {
                    height = wrapContent
                    topMargin = dip(60)
                }
                floatingActionButton {
                    id = ID_SCAN_BTN
                    imageResource = R.mipmap.icon_poisearch
                }.lparams(width = wrapContent, height = wrapContent) {
                    gravity = Gravity.BOTTOM or Gravity.CENTER
                    margin = dip(30)
                }
            }
        }
    }

    companion object {
        val ID_BT_DEVICES_LIST: Int = 0
        val ID_SCAN_BTN: Int = 1
        val ID_TITLE: Int = 2
        val ID_BACK_BTN: Int = 3
    }
}