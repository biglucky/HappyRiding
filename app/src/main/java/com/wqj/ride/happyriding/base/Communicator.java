package com.wqj.ride.happyriding.base;

public interface Communicator {
    void send(byte[] data);
    void receive(byte[] data);
    void disconnect();
    boolean isConnected();
}
