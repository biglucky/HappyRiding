package com.wqj.ride.happyriding.utils;

import com.amap.api.maps.model.LatLng;

public class Constants {
	public static final int SEARCH_RESULT_CODE = 200;

	public static final int SEARCH_ARROUND_RESULT_CODE = 300;

	public static final int SEARCH_END_REQUEST_CODE = 401;
	public static final int SEARCH_PASS_REQUEST_CODE0 = 402;
	public static final int SEARCH_PASS_REQUEST_CODE7 = 409;
	public static final int SEARCH_LOCATION_RESULT_CODE = 500;

	public static final String EXTRA_CURRENT_CITY = "city";
	public static final String EXTRA_KEYWORD = "keyword";
	public static final String EXTRA_HINT = "hint";
	public static final String EXTRA_POI = "poi";
	public static final String EXTRA_POIITEM = "poiitem";
	public static final String EXTRA_POINTTYPE = "pointType";

	public static final String EXTRA_CURRENT_LAT = "current_latitude";
	public static final String EXTRA_CURRENT_LNG = "current_longitude";

	public static final int EXTRA_SEACH_POI = 1;
	public static final int EXTRA_SEARCH_POI_ARROUND = 2;

	public static final int EXTRA_POI_TYPE_HOME = 5;
	public static final int EXTRA_POI_TYPE_COMPANY = 6;

	//public static final String APP_Preferences = "APP_Preferences";
	public static final String Navi_Preferences = "Navi_Preferences";
	public static final String SP_KEY_LicensePlateNumber = "sp_LicensePlateNumber";
	public static final String SP_KEY_IsRestrict = "sp_IsRestrict";
	public static final String SP_KEY_Congestion = "sp_Congestion";
	public static final String SP_KEY_Cost = "sp_Cost";
	public static final String SP_KEY_AvoidHightSpeed = "sp_AvoidHightSpeed";
	public static final String SP_KEY_HightSpeed = "sp_HightSpeed";


	public static final int History_Item_MAX_Nums = 10;
	public static final String History_Preferences = "History_Preferences";
	public static final String SP_KEY_ITEM_NUMS = "item_numbers";
	public static final String SP_KEY_CURRENT_INDEX = "current_idx";
	public static final String SP_KEY_ITEM_PREFIX = "item";

	public static final String BTDevice_Preferences = "BTDevice_Preferences";
	public static final String SP_KEY_BT_DEVICE_ADRESS = "bt-adress";
	public static final String SP_KEY_BT_DEVICE_NAME = "bt-name";
	public static final String SP_KEY_BT_IS_AUTO_CONNECT = "bt-autoconnect";



}
