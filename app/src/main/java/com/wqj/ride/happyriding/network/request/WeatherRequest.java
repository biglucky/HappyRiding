package com.wqj.ride.happyriding.network.request;

import com.wqj.ride.happyriding.network.response.WeatherResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WeatherRequest {
    public static String HOST = "http://www.weather.com.cn/data/sk/"; // 中国天气网

    @GET("{cityId}.html")
    Observable<WeatherResponse> getWeather(@Path("cityId") String cityId);
}
