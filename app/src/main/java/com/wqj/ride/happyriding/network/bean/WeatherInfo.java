package com.wqj.ride.happyriding.network.bean;

public class WeatherInfo {
    private String city;     // 城市名
    private String cityid;   // 城市id
    private String temp;     // 温度
    private String WD;       // 风向
    private String WS;       // 风力
    private String SD;       // 湿度
    private String AP;       // 气压
    private String njd;      // 实况
    private String WSE;
    private String time;     // 时间
    private String sm;
    private String isRader;
    private String Rader;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getWD() {
        return WD;
    }

    public void setWD(String WD) {
        this.WD = WD;
    }

    public String getWS() {
        return WS;
    }

    public void setWS(String WS) {
        this.WS = WS;
    }

    public String getSD() {
        return SD;
    }

    public void setSD(String SD) {
        this.SD = SD;
    }

    public String getAP() {
        return AP;
    }

    public void setAP(String AP) {
        this.AP = AP;
    }

    public String getNjd() {
        return njd;
    }

    public void setNjd(String njd) {
        this.njd = njd;
    }

    public String getWSE() {
        return WSE;
    }

    public void setWSE(String WSE) {
        this.WSE = WSE;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSm() {
        return sm;
    }

    public void setSm(String sm) {
        this.sm = sm;
    }

    public String getIsRader() {
        return isRader;
    }

    public void setIsRader(String isRader) {
        this.isRader = isRader;
    }

    public String getRader() {
        return Rader;
    }

    public void setRader(String rader) {
        Rader = rader;
    }

    @Override
    public String toString() {
        return "WeatherInfo{" +
                "city='" + city + '\'' +
                ", cityid='" + cityid + '\'' +
                ", temp='" + temp + '\'' +
                ", WD='" + WD + '\'' +
                ", WS='" + WS + '\'' +
                ", SD='" + SD + '\'' +
                ", AP='" + AP + '\'' +
                ", njd='" + njd + '\'' +
                ", WSE='" + WSE + '\'' +
                ", time='" + time + '\'' +
                ", sm='" + sm + '\'' +
                ", isRader='" + isRader + '\'' +
                ", Rader='" + Rader + '\'' +
                '}';
    }
}

