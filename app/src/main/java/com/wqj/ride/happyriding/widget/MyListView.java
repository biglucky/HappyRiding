package com.wqj.ride.happyriding.widget;

import android.content.Context;
import android.content.res.TypedArray;

import android.support.constraint.ConstraintLayout;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.wqj.ride.happyriding.R;

/**
 * TODO: document your custom view class.
 */
public class MyListView extends ConstraintLayout {

    public MyListView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public MyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public MyListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        String value;
        View v = LayoutInflater.from(context).inflate(R.layout.sample_my_list_view, this);

        TextView titleView = v.findViewById(R.id.titleView);
        TextView subTitleView = v.findViewById(R.id.subTitleView);
        TextView contentView = v.findViewById(R.id.contentView);

        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.MyListView, defStyle, 0);

        if (a.hasValue(R.styleable.MyListView_title)) {
            value = a.getString(R.styleable.MyListView_title);
            titleView.setText(value);
        }
        if (a.hasValue(R.styleable.MyListView_subTitle)) {
            value = a.getString(R.styleable.MyListView_subTitle);
            subTitleView.setText(value);
        }
        if (a.hasValue(R.styleable.MyListView_content)) {
            value = a.getString(R.styleable.MyListView_content);
            contentView.setText(value);
        }
        a.recycle();


    }



}

