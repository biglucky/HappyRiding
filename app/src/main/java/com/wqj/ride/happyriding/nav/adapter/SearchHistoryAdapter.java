package com.wqj.ride.happyriding.nav.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wqj.ride.happyriding.R;

import java.util.List;

/**
 *
 */
public class SearchHistoryAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mListHisory;
    private LayoutInflater layoutInflater;

    public SearchHistoryAdapter(Context context, List<String> history) {
        mContext = context;
        mListHisory = history;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (mListHisory != null) {
            return mListHisory.size();
        }
        return 0;
    }


    @Override
    public Object getItem(int i) {
        if (mListHisory != null) {
            return mListHisory.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            Holder holder;
            if (view == null) {
                holder = new Holder();
                view = layoutInflater.inflate(R.layout.history_item, null);
                holder.mName = (TextView) view.findViewById(R.id.name);
                //holder.mAddress = (TextView) view.findViewById(R.id.adress);
                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }
            if (mListHisory == null) {
                return view;
            }

            holder.mName.setText(mListHisory.get(i));
           // String address = mListTips.get(i).getAddress();
//            if (TextUtils.isEmpty(address)) {
//                holder.mAddress.setVisibility(View.GONE);
//            } else {
//                holder.mAddress.setVisibility(View.VISIBLE);
//                holder.mAddress.setText(address);
//            }
        } catch (Throwable e) {
        }
        return view;
    }

    class Holder {
        TextView mName;
        //TextView mAddress;
    }
}
