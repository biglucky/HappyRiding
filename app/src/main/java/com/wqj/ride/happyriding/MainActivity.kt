package com.wqj.ride.happyriding

import android.bluetooth.BluetoothAdapter
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.view.ViewManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.wqj.ride.happyriding.base.CheckPermissionsBaseActivity
import com.wqj.ride.happyriding.widget.MyTabView
import com.wqj.ride.happyriding.info.fragment.MyInfoFragment
import com.wqj.ride.happyriding.nav.activity.RouteShowActivity
import com.wqj.ride.happyriding.rideroad.fragment.RideRoadsFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : CheckPermissionsBaseActivity()
        , MyInfoFragment.OnFragmentInteractionListener
        , RideRoadsFragment.OnFragmentInteractionListener {

    val ID_FRAGMENT_CONTAINER = 1
    lateinit var fc: FrameLayout
    lateinit var tab1: MyTabView
    lateinit var tab2: MyTabView
    lateinit var tab3: MyTabView

    public inline fun ViewManager.myTabView() = myTabView {}
    public inline fun ViewManager.myTabView(init: MyTabView.() -> Unit)
            = ankoView({ MyTabView(it) }, 0, init)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)
        MainUi().setContentView(this@MainActivity)

        tabClick(FRAGEMENT_MY_INFO)
    }

    inner class MainUi :AnkoComponent<MainActivity> {
        override fun createView(ui: AnkoContext<MainActivity>): View {
            return with(ui) {
                verticalLayout {
                   backgroundColor = Color.parseColor("#666666")

                    fc = frameLayout {
                        id = ID_FRAGMENT_CONTAINER
                    }.lparams(width = matchParent){
                        weight = 1f
                    }

                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        backgroundColor = Color.parseColor("#ffffff")

                        tab1 = myTabView {
                            // backgroundColor = Color.parseColor("#666666")
                            text = "路线推荐"
                            normalDrawable = ContextCompat.getDrawable(ui.ctx
                                    , R.mipmap.tab_bar_ranking_g)
                            selectedDrawable = ContextCompat.getDrawable(ui.ctx
                                    , R.mipmap.tab_bar_ranking_y)
                            isSelected = false
                            onClick {
                                /*
                                val manager = supportFragmentManager
                                val transaction = manager.beginTransaction()
                                val fragment = RidingFragment()
                                transaction.replace(ID_FRAGMENT_CONTAINER, fragment)
                                transaction.commit()
                                */
                                tabClick(FRAGEMENT_RIDE_ROADS)
                            }
                        }

                        tab2 = myTabView {
                           // backgroundColor = Color.parseColor("#00000000")
                            text = "我的信息"
                            normalDrawable = ContextCompat.getDrawable(ui.ctx
                                    , R.mipmap.tab_bar_home_g)
                            selectedDrawable = ContextCompat.getDrawable(ui.ctx
                                    , R.mipmap.tab_bar_home_y)
                            isSelected = true
                            onClick {
                                /*
                                isSelected = true
                                val manager = supportFragmentManager
                                val transaction = manager.beginTransaction()
                                val fragment = MyInfoFragment()
                                transaction.replace(ID_FRAGMENT_CONTAINER, fragment)
                                transaction.commit()
                                */
                                tabClick(FRAGEMENT_MY_INFO)
                            }
                        }

                        tab3 = myTabView {
                            //backgroundColor = Color.parseColor("#00000000")
                            //background = ContextCompat.getDrawable(ui.ctx, R.mipmap.day_tab_a)
                            text = "开始骑行"
                            normalDrawable = ContextCompat.getDrawable(ui.ctx
                                    , R.mipmap.tab_bar_ride1)
                            selectedDrawable = ContextCompat.getDrawable(ui.ctx
                                    , R.mipmap.tab_bar_ride5)
                            isSelected = false
                            onClick {
                                /*
                                val manager = supportFragmentManager
                                val transaction = manager.beginTransaction()
                                val fragment = StartRideFragment()
                                transaction.replace(ID_FRAGMENT_CONTAINER, fragment)
                                transaction.commit()
                                */
                                // tabClick(3)
                                startActivity<RouteShowActivity>()
                            }
                        }
                    }.lparams(width = matchParent, height = dip(60)) {
                    }
                }
            }
        }
    }

    fun tabClick(index: Int) {
        Log.d("wqj", "index is $index")
        val transaction = supportFragmentManager.beginTransaction()

        // unselect all textviews
        tab1?.isSelected = false
        tab2?.isSelected = false
        tab3?.isSelected = false

        when (index) {
            FRAGEMENT_RIDE_ROADS -> {
                tab1?.isSelected = true
                val fragment = RideRoadsFragment()
                transaction.replace(ID_FRAGMENT_CONTAINER, fragment)
            }
            FRAGEMENT_MY_INFO -> {
                tab2?.isSelected = true
                val fragment = MyInfoFragment()
                transaction.replace(ID_FRAGMENT_CONTAINER, fragment)

            }
            FRAGEMENT_START_RIDE -> {
                tab3?.isSelected = true
                /*
                val fragment = RidingFragment()
                transaction.replace(ID_FRAGMENT_CONTAINER, fragment)
                */

            }
            else -> {
                return
            }
        }
        transaction.commit()
    }

    override fun onFragmentInteraction(uri: Uri) {

    }

    companion object {
        val FRAGEMENT_RIDE_ROADS: Int = 0
        val FRAGEMENT_MY_INFO: Int = 1
        val FRAGEMENT_START_RIDE: Int = 2
    }
}
