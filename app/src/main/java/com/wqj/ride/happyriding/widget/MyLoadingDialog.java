package com.wqj.ride.happyriding.widget;

import android.app.DialogFragment;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wqj.ride.happyriding.R;

/**
 * Created by kyee on 17-12-15.
 */
public class MyLoadingDialog extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_loading, container);
        ImageView animationIV = (ImageView) view.findViewById(R.id.anmiationIV);
        AnimationDrawable animationDrawable = (AnimationDrawable) animationIV.getDrawable();
        //开始动画
        animationDrawable.start();
        return view;
    }
}