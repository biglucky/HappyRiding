package com.wqj.ride.happyriding.rideroad.adapter

import android.content.Context
import android.widget.BaseAdapter
import android.util.Log
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.wqj.ride.happyriding.R
import com.wqj.ride.happyriding.utils.Tool
import com.wqj.ride.happyriding.bean.DataBean
import org.jetbrains.anko.windowManager


/**
 * Created by kyee on 17-12-5.
 */
class RideRoadsListViewAdapter(var mList: ArrayList<DataBean.Route>, var context: Context) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder: MyViewHolder
        //重用view
        var v: View
        if (convertView == null) {
            holder = MyViewHolder()
            v = LayoutInflater.from(context).inflate(R.layout.ride_roads_list_item, parent, false)
            holder.textView = v.findViewById<TextView>(R.id.rrItemTextView) as TextView
            holder.imageView = v.findViewById<ImageView>(R.id.rrItemImageView) as ImageView
            //设置tag
            v.tag = holder
            Log.d("wqj", "null")
        } else {
            Log.d("wqj", "not null")
            v = convertView
            //获取tag并强转
            holder = v.tag as MyViewHolder
        }
        var width = context.windowManager.defaultDisplay.width
        var height = Tool.dip2px(context, 150f)
        Log.d("wqj", "index: " + position + "title: " + v.height)
        holder.textView.text = mList[position].title
        Picasso.with(context).load(mList[position].image).resize(width, height)?.into(holder.imageView);
        return v
    }

    override fun getItem(position: Int): Any {
        return mList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mList.size
    }

    class MyViewHolder {

        lateinit var textView: TextView
        lateinit var imageView: ImageView

    }
}