package com.wqj.ride.happyriding.bluetooth.bt.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.wqj.ride.happyriding.R
import com.wqj.ride.happyriding.bluetooth.bt.bean.BluetoothDeviceDean

/**
 * Created by kyee on 17-12-8.
 */
class BtDevicesAdapter(var mList: ArrayList<BluetoothDeviceDean>, var context: Context) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder: MyViewHolder
        //重用view
        var v: View
        if (convertView == null) {
            holder = MyViewHolder()
            v = LayoutInflater.from(context).inflate(R.layout.scaned_btdev_list_item, parent, false)
            holder.textView = v.findViewById<TextView>(R.id.textView) as TextView
            holder.connectState = v.findViewById<TextView>(R.id.connectState) as TextView
            //设置tag
            v.tag = holder
        } else {
            v = convertView
            //获取tag并强转
            holder = v.tag as MyViewHolder
        }

        //为TextView设置内容
        holder.textView.text = mList[position].bluetoothDevice.name
        if(mList[position].isContected){
            holder.connectState.setVisibility(View.VISIBLE);
            holder.connectState.text = context.getText(R.string.bt_connected)
        }else{
            holder.connectState.setVisibility(View.INVISIBLE);
            holder.connectState.text = ""
        }

        //holder.textView.background = ContextCompat.getDrawable(context, R.mipmap.item)
        return v
    }

    override fun getItem(position: Int): Any {
        return mList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mList.size
    }

    class MyViewHolder {
        lateinit var textView: TextView
        lateinit var connectState: TextView
    }
}