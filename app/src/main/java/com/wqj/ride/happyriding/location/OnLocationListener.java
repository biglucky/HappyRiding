package com.wqj.ride.happyriding.location;

import com.amap.api.location.AMapLocation;

public interface OnLocationListener {
    void onLocationUpdate(AMapLocation aMapLocation);
}
