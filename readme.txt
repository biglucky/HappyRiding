#########################################################
生成jni
进入src/main/java目录
1. javah com.wqj.ride.happyriding.image_processing.OpenCVHelper
2. javac com/wqj/ride/happyriding/image_processing/OpenCVHelper.java

#########################################################
Android studio 使用ｊｎｉ
1. 新建app/CMakeLists.txt, 配置 源文件和目标文件
2. 修改app/app.iml, 增加src/main/cpp/
3. sdk/native/jni/include 拷贝到　app/src/main/cpp
4. sdk/native/libs 拷贝到　app/src/main/jniLibs
5. build.gradle增加CMakeLists.txt的调用
